var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var redis = require('redis');


io.on('connection', function (socket) {
     var subscriber = redis.createClient();
     subscriber.subscribe("laravel_database_location");
     subscriber.on("message", function (channel, message) {
          socket.emit("location", JSON.parse(message));

     });
});

http.listen(8080, function () {
     console.log('listening on *:8080');
});



