<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LRedis;

class MapController extends Controller
{
    function mapRide(Request $request){
        $location = $request->location;
        $redis = LRedis::connection();
        $redis->publish("location",json_encode($location));
        return "published";
    }
}
