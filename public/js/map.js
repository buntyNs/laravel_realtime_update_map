
// init map
var mapProp = {
    center: new google.maps.LatLng(51.508742, -0.120850),
    zoom: 5,
};
var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

//initial position
var marker = new google.maps.Marker({
  position: new google.maps.LatLng(51.508742,-0.120850),
  map: map
 });

//connect to socket.io server
var socket = io("http://localhost:8080");
    socket.on('location', function(data) { 
      //new location     
    var new_marker_position = new google.maps.LatLng(data.lat,data.lng);
    marker.setPosition(new_marker_position);
    });




